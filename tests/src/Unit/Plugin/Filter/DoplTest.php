<?php

namespace Drupal\Tests\dopl\Unit\Plugin\Filter;

use Drupal\Core\Cache\NullBackend;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\GeneratedLink;
use Drupal\Core\Url;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * Test coverage for the Dopl filter plugin.
 *
 * @covers \Drupal\dopl\Plugin\Filter\FilterDopl
 * @group dopl
 */
class DoplTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    // Mock link generator and container. Prophecy docs are confusing and do not
    // indicate that it is possible to pass /any/ argument, which is required so
    // this uses good ol' getMock().
    $this->link_generator = $this->getMockBuilder('\Drupal\Core\Utility\LinkGeneratorInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $container = new ContainerBuilder();
    $container->set('link_generator', $this->link_generator);
    \Drupal::setContainer($container);
  }

  /**
   * Assert that the replacement value has the correct HTML.
   *
   * @param string $text
   *   The text to process.
   * @param string $url
   *   The expected URL.
   * @param string $expected
   *   The expected output.
   *
   * @dataProvider textProvider
   */
  public function testProcess($text, $url, $expected) {

    // Mock GeneratedLink object.
    $generated_link = GeneratedLink::createFromObject(Url::fromUri($url, ['absolute' => TRUE]));
    $generated_link->setGeneratedLink($expected);

    // Mock link generator methods.
    $this->link_generator->expects($this->any())
      ->method('generateFromLink')
      ->willReturn($generated_link);

    // Mock guzzle client.
    $mock = new MockHandler([
      new Response(200, [], "<!doctype html><html><head><title>Test | Drupal.org</title></head><body>HTML body</body></html>"),
    ]);
    $handler = HandlerStack::create($mock);
    $cache = new NullBackend('cache');
    $client = new Client(['handler' => $handler]);

    $filter = new Dopl([], 'filter_dopl', ['provider' => 'dopl'], $cache, $client);

    $this->assertEquals($expected, $filter->process($text, '')->getProcessedText());
  }

  /**
   * Return parameters to test.
   *
   * @return array
   *   The parameters to test.
   */
  public function textProvider() {
    return [
    ['dopl.module', 'http://drupal.org/project/dopl', '<a href="http://drupal.org/project/dopl">Test</a>'],
    ['baritk.theme', 'http://drupal.org/project/bartik', '<a href="http://drupal.org/project/bartik">Test</a>'],
    ['cod.installprofile', 'http://drupal.org/project/cod', '<a href="http://drupal.org/project/cod">Test</a>'],
    ['1.do', 'http://drupal.org/node/1', '<a href="http://drupal.org/node/1">Test</a>'],
    ['1.gdo', 'http://groups.drupal.org/node/1', '<a href="http://groups.drupal.org/node/1">Test</a>'],
    ['1.user', 'http://drupal.org/user/1', '<a href="http://drupal.org/user/1">Test</a>'],
    ['1.issue', 'http://drupal.org/node/1', '<a href="http://drupal.org/node/1">Test</a>'],
    ];
  }

}
