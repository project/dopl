<?php

namespace Drupal\dopl\Plugin\Filter;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Text filter for converting text to a link to Drupal.org projects and nodes.
 *
 * @Filter(
 *   id = "filter_dopl",
 *   title = @Translation("Drupal.org project link filter."),
 *   description = @Translation("Facilitate linking to Drupal.org projects and nodes"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class FilterDopl extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;
  /**
   * The Guzzle HTTP Client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * Initialize method.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   A cache backend to use.
   * @param \GuzzleHttp\Client $client
   *   The guzzle http client.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CacheBackendInterface $cache, Client $client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->cache = $cache;
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $configuration, $plugin_id, $plugin_definition, $container->get('cache.default'), $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('<p>Use one of the forms:<br><em>name.module</em>, <em>name.theme</em>, <em>name.translation</em>, <em>name.installprofile</em> or <em>name.project</em>, in order to link to <em>http://drupal.org/project/name</em><br>Note that a link will be generated even if the project or node does not exist.</p>
        <p>Use <em>[nid].do</em> or <em>[nid].issue</em> to link to <em>http://drupal.org/node/[nid]</em>, and <em>[nid].gdo</em> to link to <em>http://groups.drupal.org/node/[nid]</em><br>Links will only be generated for valid node IDs using this format.</p>
        <p>Use <em>name.module</em>, <em>name.theme</em>, <em>name.translation</em>, <em>name.installprofile</em> or <em>name.project</em>, to link to projects Use <em>[nid].do</em> or <em>[nid].issue</em> to link to nodes, and <em>[nid].gdo</em> to link to groups.<p>');
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    $pattern = '/(?<!\S)(\b)(\w+)(\.(module|theme|translation|installprofile|project))(\b)(\|"([^"]*)")?/';
    $text = preg_replace_callback($pattern, [$this, 'getLink'], $text);

    // Link drupal.org nodes by nid.
    $pattern = '/(?<!\S)(\b)([a-z0-9#\-]*)(\.(do|issue|gdo|user))(\b)(\|"([^"]*)")?/';
    $text = preg_replace_callback($pattern, [$this, 'getLink'], $text);

    $result = new FilterProcessResult($text);
    $result->setAttachments([
      'library' => ['dopl/dopl'],
    ]);

    return $result;
  }

  /**
   * Custom function retrieves link information for regular expression matches.
   *
   * @param array $matches
   *   An array of regular expression matches.
   *
   * @return string
   *   A valid URL
   */
  protected function getLink(array $matches = []): string {
    $link_data = [];
    $link_variables = [];
    $json = FALSE;
    if ($data = $this->cache->get('dopl:' . $matches[4] . '-' . $matches[2])) {
      $link_variables = json_decode($data->data, TRUE);
    }
    else {
      switch ($matches[4]) {
        case 'module':
        case 'theme':
        case 'translation':
        case 'installprofile':
        case 'project':
          $url = 'https://www.drupal.org/api-d7/node.json?field_project_machine_name=' . $matches[2];
          $json = TRUE;
          break;

        case 'do':
        case 'issue':
          $url = 'https://www.drupal.org/api-d7/node/' . $matches[2] . '.json';
          $json = TRUE;
          break;

        case 'user':
          $url = 'https://www.drupal.org/api-d7/user/' . $matches[2] . '.json';
          $json = TRUE;
          break;

        case 'gdo':
          $url = 'https://groups.drupal.org/node/' . $matches[2];
          break;
      }

      if ($link_data = $this->doplGetLinkData($url, $json)) {
        // If url returns a project page, we assume a project / node exists.
        $this->cache->set('dopl:' . $matches[4] . '-' . $matches[2], json_encode($link_data), CacheBackendInterface::CACHE_PERMANENT);
        $link_variables = $link_data;
      }
    }

    if (!empty($link_variables['url'])) {
      $link_variables['title'] = (array_key_exists(7, $matches)) ? $matches[7] : $link_variables['title'];
      $url = Url::fromUri($link_variables['url']);
      $url->setOptions($link_variables['options']);
      $external_link = Link::fromTextAndUrl($this->t('@title', ['@title' => $link_variables['title']]), $url)->toString();
      return $link_variables['prefix'] . $external_link . $link_variables['postfix'];
    }

    return $matches[0];
  }

  /**
   * Custom function to collect data from url.
   *
   * @param string $filename
   *   The url of the page to attempt to retrieve.
   * @param string|bool $json
   *   Is the format request in JSON.
   *
   * @return string|bool
   *   An array of link data to use or FALSE if not found.
   */
  protected function doplGetLinkData($filename, $json = FALSE) {
    try {
      $response = $this->client->get($filename);
      if ($json) {
        $options = [];
        $title = NULL;
        $url = NULL;
        $prefix = '';
        $postfix = '';
        $data = json_decode($response->getBody()->getContents(), TRUE);

        // Process nodes.
        if (!empty($data['title'])) {
          $title = trim($data['title']);
        }
        if (!empty($data['url'])) {
          $url = trim($data['url']);
        }

        // Process users.
        if (!empty($data['uid'])) {
          if (!empty($data['name'])) {
            $title = trim($data['name']);
          }
        }

        // Process issues.
        if (!empty($data['type']) && $data['type'] == 'project_issue') {
          if (!empty($data['nid'])) {
            $id = trim($data['nid']);
            $title = "#$id: " . trim($data['title']);
          }
          if (!empty($data['field_issue_status'])) {
            $status_id = trim($data['field_issue_status']);
            $status_terms = [
              1 => 'Active',
              2 => 'Fixed',
              3 => 'Closed (duplicate)',
              4 => 'Postponed',
              5 => 'Closed (won\'t fix)',
              6 => 'Closed (works as designed)',
              7 => 'Closed (fixed)',
              8 => 'Needs review',
              13 => 'Needs work',
              14 => 'Reviewed & tested by the community',
              15 => 'Patch (to be ported)',
              16 => 'Postponed (maintainer needs more info)',
              18 => 'Closed (cannot reproduce)',
            ];
            $status = $status_terms[$status_id];
          }

          // Create the wrapper for the issue styling.
          $options = [];
          $options['attributes'] = ['title' => $this->t('Status: @status', ['@status' => $status])];
          $prefix = '<span class="project-issue-issue-link project-issue-status-info project-issue-status-' . $status_id . '">';
          $postfix = '</span>';
        }
        // Process modules, themes etc.
        if (!empty($data['list'][0])) {
          if (!empty($data['list'][0]['title'])) {
            $title = trim($data['list'][0]['title']);
          }
          if (!empty($data['list'][0]['url'])) {
            $url = trim($data['list'][0]['url']);
          }
        }

        $link_data = [
          'options' => $options,
          'title' => $title,
          'url' => $url,
          'prefix' => $prefix,
          'postfix' => $postfix,
        ];
        return $link_data;
      }
      // Groups.
      elseif (preg_match('#<title>(.+)</title>#iU', $response->getBody()->getContents(), $title)) {
        $link_data = [
          'options' => [],
          'title' => substr(trim($title[1]), 0, -15),
          'url' => $filename,
          'prefix' => '',
          'postfix' => '',
        ];
        return $link_data;
      }
    }
    catch (RequestException $e) {
      watchdog_exception('dopl', $e);
      return FALSE;
    }
    return FALSE;
  }

}
